let btnChangeTheme = document.createElement("button");
btnChangeTheme.type = "button";
btnChangeTheme.textContent = "Change Theme";
btnChangeTheme.style.cssText =
  "font-size:15px;padding:10px; position: relative; left:1150px;bottom:110px";
document.querySelector(".page-title-link").after(btnChangeTheme);
let accent = document.querySelector(".accent-color");

if (localStorage.getItem("theme")) {
  btnClick();
}
btnChangeTheme.addEventListener("click", btnClick)

function btnClick() {
    btnChangeTheme.classList.toggle("active");
    document.body.classList.toggle("active");
    if (
      document.body.classList.contains("active") &&
      btnChangeTheme.classList.contains("active")
    ) {
      localStorage.setItem("theme", "active");
    } else {
      localStorage.clear();
    }
}

